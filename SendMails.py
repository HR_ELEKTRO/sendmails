#! python
import smtplib
import getpass
import mimetypes
import email
import email.mime.application
import email.mime.multipart
import email.mime.text
from openpyxl import load_workbook

while True:
    file = input('Geef bestandsnaam van cijferlijst: ')
    try:
        wb = load_workbook(file)
        ws = wb.active
        # Misschien nog meer checken?
        if ws['A1'].value == 'Cursuscode' and ws['A6'].value == 'Nummer':
            break;
        print('De cijferlijst heeft niet het juiste formaat!')
    except:
        print('Error in bestandsnaam of formaat, probeer opnieuw: ')

cursuscode = ws['B1'].value
docent = ws['B2'].value
docentcode= ws['B3'].value
datum=ws['B4'].value

server = smtplib.SMTP_SSL('smtp.hro.nl')
while True:
	password = getpass.getpass('Password to send mail via '  + docentcode +  ': ')
	try:
		server.login(docentcode, password)
		break;
	except:
		print('Incorrect user and password combination, try again')
#server.set_debuglevel(True)

for row in range(7, ws.max_row):
    nummer = ws['A' + str(row)].value
    if nummer is not None:
        naam = ws['B' + str(row)].value
        voornaam = ws['C' + str(row)].value
        cijfer = ws['D' + str(row)].value
        eindcijfer = str(int(round(cijfer,0)))
        print('Mail aan student ' + voornaam + ' ' + naam + ' met cijfer ' + eindcijfer + ' verstuurd.')
        to = nummer + '@hr.nl'
        from_ = docentcode + '@hr.nl'
        subject =  'Resultaat toets ' + cursuscode
        message = 'Beste ' + voornaam + '\n\n' + 'Je hebt voor de toets ' + cursuscode + ' van ' + datum.strftime('%d-%m-%Y') + ' een ' + eindcijfer + ' behaald.\n\nMet vriendelijke groet,\n\n' + docent + '\n'
        msg = email.mime.multipart.MIMEMultipart()
        msg['Subject'] = subject
        msg['To'] = to
        msg['From'] = from_
        body = email.mime.text.MIMEText(message)
        msg.attach(body)
        server.sendmail(from_, to, msg.as_string())
server.quit()


