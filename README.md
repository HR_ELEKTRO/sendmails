# SendMails #

Het script SendMails.py verstuurt persoonlijke mails naar studenten met als invoer een cijferlijstje in Excel formaat. 

Het cijferlijstje dient opgemaakt te zijn als gegeven in het bestand voorbeeld.xlsx.
De volgende cellen worden uitgelezen uit het Excel bestand:

* B1 = De cursuscode (bijvoorbeeld: `ENGCPL01`)
* B2 = De naam van de docent (bijvoorbeeld: `Harry Broeders`)
* B3 = De code van de docent (bijvoorbeeld: `BroJZ`)
* B4 = De toetsdatum (bijvoorbeeld: `1-1-2016`)
* Voor n = 1 t/m Aantal studenten
	* A<n+6> = Nummer van student #<n> (Bijvoorbeeld: `0900001`)
	* C<n+6> = Voornaam van student #<n> (Bijvoorbeeld: `Kees`)
	* D<n+6> = Cijfer van student #<n> (Bijvoorbeeld: `10`)

## Installatie ##
Uiteraard moet Python geïnstalleerd zijn. 
Het script is getest met Python 3.5.1.
Om het Excel bestand in te lezen is gebruik gemaakt van [OpenPyXL](https://openpyxl.readthedocs.org/).
Deze library moet apart geïnstalleerd worden met het commando:
```
pip install openpyxl
```
## Voorbeeld ##
Om het script te testen kun je in het bestand voorbeeld.xlsx je eigen docentcode en naam invullen. Gebruik geen bestaande studentnummers om te testen, maar de docentcode van een collega die je wilt spammen :wink:

Als docent BroJZ het script uitvoert, verschijnt de volgende uitvoer:

```
#!text
C:\BroJZgit\HR_ELEKTRO\sendmails>SendMails.py
Geef bestandsnaam van cijferlijst: Voorbeeld.xlsx
Password to send mail via BroJZ:
Mail aan student Harry Broeders met cijfer 10 verstuurd.
Mail aan student Student Onbekend met cijfer 1 verstuurd.
```

De 'student' BroJZ ontvangt nu de volgende mail:

```
#!text
From:    <BroJZ@hr.nl>
To:      j.z.m.broeders@hr.nl
Subject: Resultaat toets ELEXXX0Y

Beste Harry

Je hebt voor de toets ELEXXX0Y van 01-01-2016 een 10 behaald.

Met vriendelijke groet,

Harry Broeders
```

De mail wordt ook verstuurd naar de niet bestaande student met nummer `1111111`. De verzender BroJZ ontvangt de volgende mail.

```
#!text
From:    Mail Delivery System <MAILER-DAEMON@smtp2.hro.nl>
To:      j.z.m.broeders@hr.nl
Subject: Undelivered Mail Returned to Sender

This is the mail system at host smtp2.hro.nl.

I'm sorry to have to inform you that your message could not
be delivered to one or more recipients. It's attached below.

For further assistance, please send mail to postmaster.

If you do so, please include this problem report. You can
delete your own text from the attached returned message.

                  The mail system

<1111111@hrnl.onmicrosoft.com>: host
   hrnl.mail.protection.outlook.com[213.199.154.23] said: 550 5.4.1
   [1111111@hrnl.onmicrosoft.com]: Recipient address rejected: Access denied
   (in reply to RCPT TO command)
```

## Tot slot ##

Dit programma is voor verbetering vatbaar.
Zie de reeds aangemaakte [issues](https://bitbucket.org/HR_ELEKTRO/sendmails/issues).
Verdere op- en aanmerkingen zijn welkom.
Maakt een [issue](https://bitbucket.org/HR_ELEKTRO/sendmails/issues) aan of mail [Harry Broeders](mailto:BroJZ@hr.nl).